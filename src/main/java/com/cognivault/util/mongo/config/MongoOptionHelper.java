package com.cognivault.util.mongo.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLSocketFactory;

import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientOptions.Builder;
import com.mongodb.ReadPreference;
import com.mongodb.Tag;
import com.mongodb.TagSet;
import com.mongodb.WriteConcern;

public class MongoOptionHelper {
	static Set<String> generalOptionsKeys = new HashSet<>();
	static Set<String> authKeys = new HashSet<>();
	static Set<String> readPreferenceKeys = new HashSet<>();
	static Set<String> writeConcernKeys = new HashSet<>();
	static Set<String> allKeys = new HashSet<>();
	
	static{
		generalOptionsKeys.add("replicaset");
		generalOptionsKeys.add("ssl");
		generalOptionsKeys.add("connecttimeoutms");
		generalOptionsKeys.add("sockettimeoutms");
		generalOptionsKeys.add("maxpoolsize");
		generalOptionsKeys.add("minpoolsize");
		generalOptionsKeys.add("maxidletimems");
		generalOptionsKeys.add("waitqueuemultiple");
		generalOptionsKeys.add("waitqueuetimeoutms");
		
		writeConcernKeys.add("w");
		writeConcernKeys.add("wtimeout");
		writeConcernKeys.add("j");
		
		readPreferenceKeys.add("slaveok");
		readPreferenceKeys.add("maxstalenessseconds");
		readPreferenceKeys.add("readpreference");
		readPreferenceKeys.add("readpreferencetags");
		
		authKeys.add("authmechanism");
		authKeys.add("authsource");
		authKeys.add("gssapiservicename");
		
		allKeys.addAll(generalOptionsKeys);
		allKeys.addAll(authKeys);
		allKeys.addAll(readPreferenceKeys);
		allKeys.addAll(writeConcernKeys);
	}
	
	public static Builder buildOptions(Map<String, List<String>> optionsMap, MongoClientOptions.Builder builder){
		for(String key : generalOptionsKeys){
			String value = getLastValue(optionsMap, key);
			if(value==null){
				continue;
			}
			switch (key){
			case "maxpoolsize":
				builder.connectionsPerHost(Integer.parseInt(value));
				break;
			case "minpoolsize":
				builder.minConnectionsPerHost(Integer.parseInt(value));
				break;
			case "waitqueuemultiple":
				builder.threadsAllowedToBlockForConnectionMultiplier(Integer.parseInt(value));
				break;
			case "waitqueuetimeoutms":
				builder.maxWaitTime(Integer.parseInt(value));
				break;
			case "connecttimeoutms":
				builder.connectTimeout(Integer.parseInt(value));
				break;
			case "sockettimeoutms":
				builder.socketTimeout(Integer.parseInt(value));
				break;
			case "ssl":
				if(_parseBoolean(value))
					builder.socketFactory(SSLSocketFactory.getDefault());
				break;
			}
		}
		
		WriteConcern writeConcern = createWriteConcern(optionsMap);
		ReadPreference readPreference = createReadPreference(optionsMap);
		
		if(writeConcern != null)
			builder.writeConcern(writeConcern);
		if(readPreference != null)
			builder.readPreference(readPreference);
		
		return builder;
	}
	public static Map<String, List<String>> parseOptions(String optionsPart){
		Map<String, List<String>> optionMap = new HashMap<>();
		
		for(String _part : optionsPart.split("&|;")){
			int idx = _part.indexOf('=');
			if(idx>=0){
				String key=_part.substring(0,idx).toLowerCase();
				String value=_part.substring(idx+1);
				List<String> valueList = optionMap.get(key);
				if(valueList==null)
					valueList=new ArrayList<>(1);
				valueList.add(value);
				optionMap.put(key, valueList);
			}
		}
		
		return optionMap;
	}
	
	private static WriteConcern createWriteConcern(final Map<String,List<String>> optionsMap){
		Boolean safe = null;
		String w=null;
		int wTimeout=0;
		boolean fsync=false;
		boolean journal=false;
		
		for(String key : writeConcernKeys){
			String value=getLastValue(optionsMap,key);
			if(value==null) continue;
			switch(key){
			case "safe":
				break;
			case "w":
				break;
			case "wtimeout":
				break;
			case "fsync":
				break;
			case "j":
				break;
			} 
		}
		return buildWriteConcern(safe,w,wTimeout,fsync,journal);
	}

	private static ReadPreference createReadPreference(Map<String, List<String>> optionsMap) {
		
		Boolean slaveOk=null;
		String readPreferenceType=null;
		List<TagSet> tagSets = new ArrayList<>();
		
		for(String key : readPreferenceKeys){
			String value = getLastValue(optionsMap,key);
			if(value==null)continue;
			
			switch (key){
			case "slaveok":
				slaveOk=_parseBoolean(value);
				break;
			case "readpreference":
				readPreferenceType=value;
				break;
			case "readpreferencetags":
				for(String cur : optionsMap.get(key)){
					TagSet tagset=getTagSet(cur.trim());
					tagSets.add(tagset);
				}
				break;
			}
		}
		
		return buildReadPreferences(readPreferenceType,tagSets,slaveOk);
	}
	private static ReadPreference buildReadPreferences(final String readPreferenceType,final List<TagSet> tagSets, final Boolean slaveOk){
		if(readPreferenceType!=null){
			if(tagSets==null){
				return ReadPreference.valueOf(readPreferenceType);
			}else{
				return ReadPreference.valueOf(readPreferenceType,tagSets);
			}
		}else if(slaveOk!=null && slaveOk.equals(Boolean.TRUE)){
			return ReadPreference.secondaryPreferred();
		}
		return null;
	}
	private static WriteConcern buildWriteConcern(final Boolean safe, final String w,
			final int wTimeout,final boolean fsync, final boolean journal){
		if(w!=null || wTimeout!=0 || fsync || journal){
			if(w==null){

				return new WriteConcern(1).withWTimeout(wTimeout, TimeUnit.MILLISECONDS).withJournal(fsync||journal);
			}else{
				try{
					return new WriteConcern(Integer.parseInt(w)).withWTimeout(wTimeout, TimeUnit.MILLISECONDS).withJournal(fsync||journal);
				}catch(NumberFormatException e){
					return new WriteConcern(w).withWTimeout(wTimeout, TimeUnit.MILLISECONDS).withJournal(fsync||journal);
				}
			}
		}else if(safe!=null){
			if(safe){
				return WriteConcern.ACKNOWLEDGED;
			}else{
				return WriteConcern.UNACKNOWLEDGED;
			}
		}
		return null;
	}
	private static TagSet getTagSet(String tagSetString){
		List<Tag> tagList = new ArrayList<>();
		if(tagSetString.length()>0){
			for(String tag : tagSetString.split(",")){
				String[] tagKeyValuePair = tag.split(":");
				if(tagKeyValuePair.length != 2){
					throw new IllegalArgumentException("Bad read preference tags: " + tagSetString);
				}
				tagList.add(new Tag(tagKeyValuePair[0].trim(), tagKeyValuePair[1].trim()));
			}
		}
		return new TagSet(tagList);
	}
	static boolean _parseBoolean(String _in){
		String in =_in.trim();
		return in!=null && in.length()>0 &&(in.equals("1") || in.toLowerCase().equals("true")||in.toLowerCase().equals("yes"));
	}
	private static String getLastValue(final Map<String, List<String>> optionsMap, final String key){
		List<String> valueList=optionsMap.get(key);
		if(valueList==null){
			return null;
		}
		return valueList.get(valueList.size()-1);
	}
}
