package com.cognivault.util.mongo.config;

import static com.cognivault.util.mongo.config.MongoOptionHelper.buildOptions;
import static com.cognivault.util.mongo.config.MongoOptionHelper.parseOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.core.WriteResultChecking;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;

@Configuration
@EnableConfigurationProperties(CGMongoConfigurationProperties.class)
public class CGSpringMongoConfiguration extends AbstractMongoConfiguration{

	@Value("${mongo.hostname}")
	private String hostname;
	@Value("${mongo.databasename}")
	private String databasename;
	@Value("${mongo.username}")
	private String username;
	@Value("${mongo.password}")
	private String password;
	@Value("${mogno.options}")
	private String optionstring;
	
	@Bean
	public Mongo mongo(){
		return getClient();
	}
	
	@Override
	@Bean
	public MongoTemplate mongoTemplate() throws Exception{
		MongoTemplate template = new MongoTemplate(mongoDbFactory(),mappingMongoConverter());
		template.setWriteResultChecking(WriteResultChecking.EXCEPTION);
		return template;
	}
	
	@Bean
	public MongoDbFactory mongoDbFactory(){
		return new SimpleMongoDbFactory(getClient(),databasename);
	}
	
	@Override
	protected String getDatabaseName(){
		return databasename;
	}
	
	private MongoClient getClient(){
		MongoClientOptions options = buildOptions(parseOptions(optionstring),MongoClientOptions.builder()).build();
		return new MongoClient(getHost(),getCredential(),options);
	}
	
	private List<ServerAddress> getHost(){	
		List<ServerAddress> result = new ArrayList<>();
		for(String s : hostname.split(",")){
			result.add(new ServerAddress(s));
		}
		return result;
	}
	
	private List<MongoCredential> getCredential(){
		if(username.isEmpty()&&password.isEmpty())
			return Arrays.asList();
		MongoCredential credential = 
				MongoCredential.createCredential(username, getDatabaseName(), password.toCharArray());
		return Arrays.asList(credential);
	}
}
