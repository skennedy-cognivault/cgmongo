package com.cognivault.util.mongo.config;

import javax.validation.constraints.NotBlank;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix="mongo")
public class CGMongoConfigurationProperties {
	
	@NotBlank
	private String hostname;
	@NotBlank
	private String databasename;
	/**
	 * Defaults to empty
	 */
	private String username;
	/**
	 * Defaults to empty
	 */
	private String password;
	/**
	 * Defaults to empty
	 */
	private String options;
	
	public String getHostname() {
		return hostname;
	}
	public void setHostname(String hostname) {
		this.hostname = hostname;
	}
	public String getDatabasename() {
		return databasename;
	}
	public void setDatabasename(String databasename) {
		this.databasename = databasename;
	}
	/**
	 * default is empty
	 * @return 
	 */
	public String getUsername() {
		return username==null ? username:"";
	}
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * default is empty
	 * @return 
	 */
	public String getPassword() {
		return password==null ? password:"";
	}
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * default is empty
	 * @return 
	 */
	public String getOptions() {
		return options==null ? options:"";
	}
	public void setOptions(String options) {
		this.options = options;
	}
}
